$(document).ready(function(){
    $('.menu__wrap-item').mouseover(function(){
        $(this).find('.menu__item-submenu').addClass('active');
        $(this).find('.menu__item.sub').addClass('open');
    });

    $('.menu__wrap-item').mouseout(function(){
        $(this).find('.menu__item-submenu').removeClass('active');
        $(this).find('.menu__item.sub').removeClass('open');
    });


    $('#search').focus(function(){
        $('.menu__search-wrap-input').addClass('enter');
    });

    $('#search').focusout(function(){
        if($(this).val() == '') {
            $('.menu__search-wrap-input').removeClass('enter');
        }
    });




});

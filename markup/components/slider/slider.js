$(document).ready(function(){
    $('.js-slider__content').slick({
        dots: false,
        autoplay: true,
        autoplaySpeed: 3000,
        infinite: true,
        arrows: false,
        draggable: true,
        slidesToShow: 1,
        swipeToSlide: true,
    });
    var allSlide = $('.slider__item').length;

    $('#js-curent-slide').text(allSlide);
    $('.js-slider__content').on('afterChange', function(event, slick, currentSlide, nextSlide){
        $('#js-curent-slide').text(currentSlide+1);
        $('.js-slider__tab-item').each(function(index) {
            if($(this).attr('data-slide') == currentSlide) {
                $('.slider__tab-item').removeClass('active');
                $(this).addClass('active');
            }
        });
    });



    $('.js-slider__tab-item').on('click', function(e) {
        e.preventDefault();
        var setSlide = $(this).attr('data-slide');
        $('.slider__tab-item').removeClass('active');
        $(this).addClass('active');
        $('.js-slider__content').slick('slickGoTo', setSlide);
    });

    $('#js-slide-prev').on('click', function(e) {
        e.preventDefault();
        $('.js-slider__content').slick('slickPrev');
    });

    $('#js-slide-next').on('click', function(e) {
        e.preventDefault();
        $('.js-slider__content').slick('slickNext');
    });

    // #js-all-slide
    // #js-curent-slide

});
